module.exports = {
    entry:  './src/index.js',
    output: {
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',

                query: {
                    presets: [ 'es2015', 'stage-3', 'react']

                }

            }

        ]
    }
};