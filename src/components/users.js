import React, {Component} from 'react';
import Button from './Button';
import UserItem from './useritem';
//
class Users extends Component {
    constructor(props) {
        super(props);
    }



    render() {
        const {data,fetchUsers} =this.props;
        console.log(data);
        return (
            <div className="container">
			<Button
				onClick={fetchUsers}
				text={'Fetch Users'}
			    className={'btn btn-blue'}
			/>
	            <div id="user"></div>
	            {data.users.map((usr, i) => {
				return <UserItem
						key={i}
				        user={usr}
				/>
	            })
	            }
            </div>
        );
    }
}

export default Users;