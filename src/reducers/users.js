// Promise
// pending
// fulfilled
// rejected
const initalState = {
    sendingRequest: false,
    requestRecieved: false,
    user: {
        name: '',
        email: '',
        gender: ''
    },
    status: '',
    statusClass: ''
};

// REDCUER
function userReducer(state=initalState, action) {
    let users;

    switch (action.type) {
        case 'FETCH_USER_PENDING':
            return {...state,loading: true};
            break;
        case 'FETCH_USER_FULFILLED':
            users = action.payload.data.results;

            return {...state, loading: false, users};
            break;
        case 'FETCH_USER_REJECTED':
            return {...state, loading: false, error: `${action.payload.message}`, users};
        default:
            return state;
    }
}

export default userReducer;